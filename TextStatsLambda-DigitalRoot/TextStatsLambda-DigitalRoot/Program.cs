﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextStatsLambda_DigitalRoot
{
    class Program
    {
        static void Main(string[] args)
        {
            const string test = "Well I feel like there could be anything in the world written here, and you would still read it in Morgan Freeman's voice. Amazing thing this concept.";
            Console.WriteLine("Input: {0}", test);
            Console.WriteLine("Number of characters: {0}", test.Count());
            Console.WriteLine("Number of words: {0}", test.Split(' ').Count());
            Console.WriteLine("Number of consonants: {0}", NumberOfConsonants(test));
            Console.WriteLine("Number of vowels: {0}", NumberOfVowels(test));
            Console.WriteLine("Number of special characters: {0}", NumberOfSpecialCharacters(test));
            Console.WriteLine("Longest word: {0}", LongestWord(test));
            Console.WriteLine("Shortest word: {0}", ShortestWord(test));

            Console.ReadKey();
        }

        public static int DigitalRoot(string rootThisNumber)
        {
            return rootThisNumber.Sum(number => int.Parse(number.ToString())) > 9 ? rootThisNumber.Sum(number => int.Parse(number.ToString())).ToString().Sum(root => int.Parse(root.ToString())) : rootThisNumber.Sum(number => int.Parse(number.ToString()));
        }

        public static int NumberOfWords(string inputString)
        {
            return inputString.Split(' ').Count();
        }

        public static int NumberOfVowels(string inputString)
        {
            return inputString.Count(x => "aeiou".Contains(x.ToString().ToLower()));
        }

        public static int NumberOfConsonants(string inputString)
        {
            return inputString.Count(x => "bcdfghjklmnpqrstvwxyz".Contains(x.ToString().ToLower()));
        }

        public static int NumberOfSpecialCharacters(string inputString)
        {
            // .,?;'!@#$%^&*() and spaces are considered special characters
            return inputString.Count(x => ".,?;'!@#$%^&*() ".Contains(x));
        }

        public static string LongestWord(string inputString)
        {
            return inputString.Split(' ').OrderByDescending(x => x.Length).First();
        }

        public static string ShortestWord(string inputString)
        {
            return inputString.Split(' ').OrderBy(x => x.Length).First();
        }


    }
}
